(* ------------ declaration des types: ------------- *)
module type LABEL =
  sig
    type t
    val compare : t -> t -> int
    val of_int : int -> t
    val print : Format.formatter -> t -> unit
    val null : t
  end

module type GRAPH_DIGRAPH_BASIC =
  sig
    module L : LABEL
    type t
    val empty : t
    val add_node : L.t -> t -> t
    val add_edge : L.t * L.t -> t -> t
    val remove_edge : L.t * L.t -> t -> t
    val is_adj : L.t -> L.t -> t -> bool
    val vertices : t -> L.t list
    val filter : (L.t -> bool) -> t -> t
  end

module type GRAPH_BASIC =
  sig
    include GRAPH_DIGRAPH_BASIC
    val neighboorhood : L.t -> t -> L.t list
  end

module type GRAPH =
  sig
    include GRAPH_BASIC
    val degree : L.t -> t -> int
    val read_one : in_channel -> t
    val write_one : out_channel -> t -> unit
    val print : Format.formatter -> t -> unit
    val is_stable : t -> bool
    val is_clique : t -> bool
    val has_cycle : t -> bool
    val edges : t -> (L.t * L.t) list
    val arboricity : t -> int
    val get_biforest : t -> L.t list
    val is_cycle: t -> bool
    val get_one_cc: t -> L.t -> L.t list
    val is_connected: t -> bool
    val get_cut_nodes: t -> L.t list
    val get_2cc: t -> t list
    val get_cc: t -> t list
    val is_cactus: t -> bool
    val to_latex: Format.formatter -> t -> unit
  end

module type DIGRAPH_BASIC =
  sig
    include GRAPH_DIGRAPH_BASIC
    val out_vertices : L.t -> t -> L.t list
    val in_vertices : L.t -> t -> L.t list
  end

module type DIGRAPH =
  sig
    include DIGRAPH_BASIC
    val print : Format.formatter -> t -> unit
    val out_degree : L.t -> t -> int
    val in_degree : L.t -> t -> int
    val degree : L.t -> t -> int
    val min_out_degree : t -> int
    val min_in_degree : t -> int
    val edges: t -> (L.t * L.t) list
    val read_one : in_channel -> t
    val write_one : out_channel -> t -> unit
    val has_cycle : t -> bool
    val is_kcol : int -> t -> bool
    val reverse : t -> t
    val digirth : t -> int
    val to_latex: Format.formatter -> t -> unit
    val get_cc: t -> t list
    val get_2cc: t -> t list
    val is_connected: t -> bool
    val is_cycle: t -> bool
    val is_cactus: t -> bool
    val is_kcritical: int -> t -> bool
  end

(* -------------- fonctions utiles: ---------------------- *)
let ceiling x d = 
  if x mod d = 0 then x / d else (x/d) + 1 

(* -------------- definitions des modules ---------------- *)

module Intlabel: LABEL with type t=int =
  struct
  type t = int
  let null = -1
  let of_int = fun x -> x
  let compare a b =  if a = b then 0 else if a<b then -1 else 1
  let print fmt x = Format.fprintf fmt "%d" x
end

module G(L: LABEL): GRAPH_BASIC with module L=L = struct
  module L = L
  module Lmap = Map.Make(L)
  module Lset = Set.Make(L)
  type t = Lset.t Lmap.t (* par list d'adjacence *)

  let empty = Lmap.empty
  let add_node v g = 
    if Lmap.mem v g then g else Lmap.add v Lset.empty g

  let vertices g = Lmap.fold (fun v s l -> v::l) g []

  let filter cond g =
    Lmap.fold 
      (fun v s g' -> 
        (if cond v then
          Lmap.add v (Lset.filter cond s) g'
         else g'))
      g empty

  let is_adj v w g =
    if v = L.null || w = L.null then false else
    Lset.mem w (Lmap.find v g)

  let neighboorhood v g = (* dans le sens successeurs *)
    let n = Lmap.find v g in
    Lset.fold (fun w l -> w::l) n []      

  let add_edge (v1,v2) g =
    let nv1 = Lset.add v2 (Lmap.find v1 g) in
    let nv2 = Lset.add v1 (Lmap.find v2 g) in
    Lmap.add v1 nv1 (Lmap.add v2 nv2 g)

  let remove_edge (v1,v2) g =
    let nv1 = Lset.remove v2 (Lmap.find v1 g) in
    let nv2 = Lset.remove v1 (Lmap.find v2 g) in
    Lmap.add v1 nv1 (Lmap.add v2 nv2 g)

end

module Gint: GRAPH_BASIC with module L=Intlabel = struct
  module L = Intlabel
  module Lmap = Map.Make(L)

  type t = int Lmap.t
  
  let nb_bits = 15 (* nb maximal de sommet ATTENTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!! *)

  let empty = Lmap.empty

  let add_node v g = Lmap.add v 0 g 

  let add_edge (v1,v2) g =
    let old_v1 = Lmap.find v1 g in
    let old_v2 = Lmap.find v2 g in
    Lmap.add v1 (old_v1 lor (1 lsl v2))
   (Lmap.add v2 (old_v2 lor (1 lsl v1)) g)

  let remove_edge (v1,v2) g =
    let old_v1 = Lmap.find v1 g in
    let old_v2 = Lmap.find v2 g in
    Lmap.add v1 (old_v1 lor (lnot (1 lsl v2)))
   (Lmap.add v2 (old_v2 lor (lnot (1 lsl v1))) g)

  let is_adj v1 v2 g =
    let adj_v1 = Lmap.find v1 g in
    0 <> (adj_v1 land (1 lsl v2))

  let vertices g = Lmap.fold (fun v s l -> v::l) g []

  let filter f g =
    let rec make_filter k = 
      if k = nb_bits - 1 then 0 else
        if f k
        then (1 lsl k) lor (make_filter (k + 1))
        else make_filter (k + 1)
    in
    let filt = make_filter 0 in
    Lmap.fold 
      (fun v s g' -> 
        (if f v then
          Lmap.add v (s land filt) g'
         else g'))
      g empty

  let neighboorhood v g =
    let rec aux k =
      if k=nb_bits-1 then
        []
      else
        if is_adj v k g then
          k::(aux (k+1))
        else 
          aux (k+1)
  in
  aux 0

end

module MakeGraph(G:GRAPH_BASIC): GRAPH with module L=G.L =
  struct
    include G

    module Lmap = Map.Make(L)
    module Lset = Set.Make(L)

    let degree v g =
      List.length (neighboorhood v g)    

    let read_one ch = (* format graph6, on suppose que |V(g)| <= 62 *)
      let nb_vertex = (input_byte ch) - 63 in
      (*Format.printf "nb_vertex = %d@." nb_vertex;*)
      let mat = input_line ch in (* triangle sup de la matrice d'adjacence *)
      let result = ref empty in
      result := add_node (L.of_int 0) (!result);
      for j=1 to nb_vertex - 1 do (* lignes *)
        result := add_node (L.of_int j) (!result);
        for i=0 to j-1 do (* colonnes *)
          let nb = j*(j-1)/2 + i in
          let index = nb mod 6 in
          (*Format.printf "coor = %d %d@." (nb / 6) (index);*)
          let character = int_of_char (String.get mat (nb / 6)) - 63 in
          (*Format.printf "char=%d@." (character+63);*)
          let matij = (character  lsr (5-index)) land 1 in
          (*Format.printf "mat(%d,%d) = %d@." i j matij;*)
          if matij = 1 
            then result := add_edge (L.of_int i, L.of_int j) (!result)
            else ()
        done;
      done;
      !result

  let write_one (ch: out_channel) (g: t): unit = (* format digraph6 *)
    let nb_vertex = (List.length (vertices g)) in
    let taille = ceiling (((nb_vertex - 1)*nb_vertex) / 2) 6 in (* nb de char necessaires *)
    let res = Array.make taille 0 in
    for i=1 to nb_vertex - 1 do
      for j=0 to i-1 do
        let v =
           if is_adj (L.of_int i) (L.of_int j) g then 1 else 0
        in
        let nb = ((i-1)*i)/2 + j in
        let index_char = nb/6 in
        let index = nb mod 6 in
        res.(index_char) <- res.(index_char) lor (v lsl (5-index));
      done;
    done;
    (*output_string ch "&";*)
    output_byte ch (63 + nb_vertex);
    Array.iter (fun c -> output_byte ch (c+63)) res;
    output_string ch "\n";
    flush ch

  let print fmt g =
    let rec print_li fmt = function
      |[] -> ()
      |x::q -> Format.fprintf fmt "%a, %a" L.print x print_li q
    in
    let rec print_li_adj fmt = function
      |[] -> ()
      |v::q ->
        Format.fprintf fmt "%a%a: %a@." 
          print_li_adj q
          L.print v 
          print_li (neighboorhood v g)
    in
    print_li_adj fmt (vertices g)

  let is_stable g = (* pas ultra efficace (on teste les aretes des deux cotes) *)
    List.for_all (fun v -> (neighboorhood v g)=[]) (vertices g)
       
  let is_clique g =
    let li_v = vertices g in
    List.for_all (fun v -> (neighboorhood v g)=li_v) li_v

  type state = NonVisited| Visited| InProgress
  exception Cycle
  let has_cycle g = 
    let states_init =
      List.fold_left (fun m v -> Lmap.add v NonVisited m) Lmap.empty (vertices g) in
    let rec aux states parent v =
      let rec parcours_n: L.t list -> state Lmap.t = function
        |[] -> states
        |w::q -> 
          if parent=w then parcours_n q else
          match Lmap.find w states with
          |Visited -> parcours_n q
          |InProgress -> raise Cycle
          |NonVisited -> 
            Lmap.add v Visited (aux (Lmap.add v InProgress (parcours_n q)) v w)
      in
      parcours_n (neighboorhood v g)
      
    in
    let rec for_all states = function
      |[] -> states
      |v::q -> aux (for_all states q) L.null v
    in
    try 
      let _ = for_all states_init (vertices g) in false 
    with Cycle -> true

  let edges g = (* attention: renvoie les aretes en doubles ! *)
    let rec aux v l = function
      |[] -> l
      |w::q -> (v,w)::(aux v l q)
    in
    List.fold_left (fun l v -> aux v l (neighboorhood v g)) [] (vertices g)

  let rec is_karb = function (* test de la k-colorabilite *)
    |0 -> fun g -> g=empty
    |1 -> fun g -> not (has_cycle g)
    |n -> fun g ->
      let rec parcours_sub (f: L.t -> bool) l =
        (* f: pour separer les ensembles; l: liste de sommets *)
        let h1 = filter f g in 
        (* si on a deja un cycle, on peut s'arreter tout de suite *)
        if has_cycle h1 then false else
        (match l with
         |[] -> (* on verifie si c'est une (k-1)-col correcte *) 
           let h2 = filter (fun x -> not (f x)) g in
           is_karb (n-1) h2
         |v::q -> 
           (parcours_sub (fun x -> (x=v) || (f x)) q)
           ||  (parcours_sub f q)
        )
      in
      match vertices g with
        |[] -> true
        |v0::li_v ->
          parcours_sub (fun v -> v=v0) li_v

  let arboricity g = 
    let k = ref 0 in
    while (not (is_karb (!k) g)) do incr k done;
    !k

  exception ArboricityGt2
  let get_biforest g = (* renvoie une paritition en deux forets *)
    let rec parcours_sub f = function
      |[] ->
        let h = filter f g in
        let h2 = filter (fun x -> not (f x)) g in
        if (has_cycle h) || (has_cycle h2) then
          raise ArboricityGt2
        else
          vertices h
      |v::q ->
        try
          let f2 = fun x -> (x=v) || (f x) in
          let h2 = filter f2 g in
          if has_cycle h2 then raise ArboricityGt2 else
          parcours_sub (fun x -> (x=v)  || (f x)) q
        with ArboricityGt2 -> 
          parcours_sub f q
    in
    parcours_sub (fun _ -> false) (vertices g)

  let rec parcours (g: t) (st: state Lmap.t) (v: L.t): (state Lmap.t) * (L.t list) =
    let inprogress_states = Lmap.add v InProgress st in
    let new_states, new_li =
      List.fold_left
       (fun (s,l) w ->
         match (Lmap.find w s) with
         |Visited |InProgress -> (s,l)
         |NonVisited ->
           let s', l' = parcours g s w in
           s', l' @ l
       )
       (inprogress_states, [])
       (neighboorhood v g)
    in
    Lmap.add v Visited new_states, v::new_li

  let get_one_cc (g: t) (v0: L.t) = 
    let states_init = List.fold_left (fun m v -> Lmap.add v NonVisited m) Lmap.empty (vertices g) in
    let _, cc = parcours g states_init v0 in
    cc 

  let get_cc (g: t): t list =
    let states_init = List.fold_left (fun m v -> Lmap.add v NonVisited m) Lmap.empty (vertices g) in
    let rec aux st = function
      |[] -> st, []
      |x::q ->
        if Lmap.mem x st then aux st q else
        let sti, cc = parcours g st x in
        let stf, lif = aux sti q in
        let cc2: t =
          List.fold_left
            (fun t x -> 
               List.fold_left (fun t' y -> if is_adj x y g then add_edge (x,y) t' else t')
                              (add_node x t) cc
            )
            empty
            cc
        in
        stf, cc2::lif
    in
    let _, r = aux states_init (vertices g) in
    r
     

  let is_connected g =
    match vertices g with
      |[] -> true (* empty graph *)
      |v0::_ -> 
        (List.length (get_one_cc g v0))=(List.length (vertices g))
     
  let is_cycle g =  (* a cycle is a 2-regular connected graph *)
    (List.for_all (fun x -> degree x g = 2) (vertices g)) &&
    (is_connected g)

  (* Tarjan's algorithm *)
  let common_cut_vertex_biconnected_components (g: t): (L.t list) * (t list) =
    let vert = vertices g in
    let nb_vertex = List.length vert in
    let depth: (L.t, int) Hashtbl.t = Hashtbl.create nb_vertex in
    let low  : (L.t, int) Hashtbl.t = Hashtbl.create nb_vertex in
    let parent  : (L.t, L.t) Hashtbl.t = Hashtbl.create nb_vertex in
    List.iter (fun x -> Hashtbl.add parent x L.null) vert;
    let cut_set: Lset.t ref = ref Lset.empty in
    let edge_stack : (L.t * L.t)      Stack.t = Stack.create () in
    let current_bcc: (L.t * L.t)      Stack.t = Stack.create () in
    let bcc_stack  : (L.t * L.t) list Stack.t = Stack.create () in
    let d = ref 0 in
    let rec aux (u: L.t): unit = 
      Hashtbl.add depth u (!d);
      Hashtbl.add low   u (!d);
      incr d;
      let children = ref 0 in
      let is_articulation = ref false in
      List.iter
       (fun v -> 
         if not (Hashtbl.mem depth v)
         then
          (
           Hashtbl.add parent v u;
           incr children;
           Stack.push (u,v) edge_stack;
           aux v;
           Hashtbl.add low u (min (Hashtbl.find low u) (Hashtbl.find low v)); 
           let has_parent = Hashtbl.mem parent u in
           (if (has_parent      && ((Hashtbl.find low v) >= (Hashtbl.find depth u))) ||
               (not has_parent) && (!children > 1)
           then 
            (
             is_articulation := true;
             (* on depile jusqu'a (i,ni) *)
             let u1 = ref L.null in
             let u2 = ref L.null in
             while (!u1, !u2) <> (u,v) do
              (if !u1 <> L.null then
                 Stack.push (!u1, !u2) current_bcc
               else ());
               let a,b = Stack.pop edge_stack in
              (*Format.eprintf "a,b= %a,%a@." L.print a L.print b;*)
               u1 := a; u2 := b;
             done;
             Stack.push (u,v) current_bcc;
             Stack.push (Stack.fold (fun li x -> x::li) [] current_bcc) bcc_stack;
             Stack.clear current_bcc
            )
           else ());
          )
         else
          (
           if (v <> (Hashtbl.find parent u)) && ((Hashtbl.find depth u) > (Hashtbl.find depth v)) 
           then
            (
             Hashtbl.add low u (min (Hashtbl.find low u) (Hashtbl.find depth v));
             (*Format.eprintf "(%a,%a)@." L.print u L.print v;*)
             Stack.push (u,v) edge_stack;
            )
           else ()
          );
       )
       (neighboorhood u g);
       if ((Hashtbl.mem parent u) && (!is_articulation)) ||
          ((not (Hashtbl.mem parent u)) && (!children > 1))
       then
        ((*Format.eprintf "node %a is a cut vertex@." L.print u;*)
         cut_set := Lset.add u (!cut_set)) 
       else () 
    in
    let rec li_edges_to_graph = function
     |[] -> empty
     |(x,y)::q -> add_edge (x,y) (add_node y (add_node x (li_edges_to_graph q)))
    in
    List.iter
     (fun v ->
        if not (Hashtbl.mem depth v) then aux v else ())
     vert;
    Lset.fold (fun x li -> x::li) (!cut_set) [],
    Stack.fold 
      (fun li bcc -> 
        let t = li_edges_to_graph bcc in
        (*Format.eprintf "%a@." print t;*) t::li)
      [] bcc_stack

  let get_cut_nodes (g: t): L.t list = (* returns the list of the cut vertices *)
    let r,_ = common_cut_vertex_biconnected_components g in
    r

  let get_2cc (g: t): t list = (* returns the biconnected components (list of graphs) *)
    let _,r = common_cut_vertex_biconnected_components g in
    r

  let is_cactus (g: t): bool = (* all biconnected components are cycles *)
    List.for_all
      (fun h -> ((List.length (vertices h))<=2) || (is_cycle h))
      (get_2cc g)

  let to_latex (fmt: Format.formatter) (g: t): unit =
     let rec add_vert (fmt: Format.formatter): L.t list -> unit = function
       |[] -> ()
       |[v] -> Format.fprintf fmt "%a" L.print v
       |v::q -> Format.fprintf fmt "%a, %a" L.print v add_vert q
     in
     let rec add_edges fmt = function
       |[] -> ()
       |(v,w)::q -> Format.fprintf fmt "  \\Edges (%a,%a)@.%a" L.print v L.print w add_edges q
     in
     Format.fprintf fmt
"\\begin{tikzpicture}[scale=3]
  \\GraphInit[vstyle=Hasse]
  \\SetVertexLabel
  \\SetGraphUnit{0.75}
  \\Vertices{circle}{%a}
  %a
\\end{tikzpicture}"
        add_vert (vertices g)
        add_edges (edges g)

end

module MakeDigraph(G1: DIGRAPH_BASIC): DIGRAPH with module L=G1.L = struct
  include G1

  module Lmap = Map.Make(L)
  module Lset = Set.Make(L)

  module G=MakeGraph(G(L))

  let print fmt g =
    let rec print_li fmt = function
      |[] -> ()
      |x::q -> Format.fprintf fmt "%a, %a" L.print x print_li q
    in
    let rec print_li_adj fmt = function
      |[] -> ()
      |v::q ->
        Format.fprintf fmt "%a%a: %a@." 
          print_li_adj q
          L.print v 
          print_li (out_vertices v g)
    in
    print_li_adj fmt (vertices g)

  let in_degree (v: L.t) (g: t): int = List.length (in_vertices v g)
  let out_degree (v: L.t) (g: t): int = List.length (out_vertices v g)
  let degree (v: L.t) (g: t): int = (in_degree v g) + (out_degree v g)

  let min_out_degree (g: t): int =
    List.fold_left (fun m v -> min m (out_degree v g)) max_int (vertices g)
  let min_in_degree (g: t): int =
    List.fold_left (fun m v -> min m (in_degree v g)) max_int (vertices g)

  let edges g = (* attention: renvoie les aretes en doubles ! *)
    let rec aux v l = function
      |[] -> l
      |w::q -> (v,w)::(aux v l q)
    in
    List.fold_left (fun l v -> aux v l (out_vertices v g)) [] (vertices g)

  let check_degree (m: int) (g: t): bool = (* verifie si \delta^0 <= d *)
    let f v = ((in_degree v g) <= m) || ((out_degree v g) <= m) in
    List.exists f (vertices g)

  let reverse (g: t): t = (* pas teste *) 
    List.fold_left
     (fun h v -> List.fold_left (fun h' v' -> add_edge (v',v) h') (add_node v h) (out_vertices v g))
     empty
     (vertices g)

(* j'ai fait un copie colle, c'est mal *)
  type state = NonVisited| Visited| InProgress
  exception Cycle
  let has_cycle g = 
    let states_init = List.fold_left (fun m v -> Lmap.add v NonVisited m) Lmap.empty (vertices g) in
    let rec aux states parent v =
      let rec parcours_n: L.t list -> state Lmap.t = function
        |[] -> states
        |w::q -> 
          if parent=w then parcours_n q else
          match Lmap.find w states with
          |Visited -> parcours_n q
          |InProgress -> raise Cycle
          |NonVisited -> 
            Lmap.add v Visited (aux (Lmap.add v InProgress (parcours_n q)) v w)
      in
      parcours_n (out_vertices v g)
      
    in
    let rec for_all states = function
      |[] -> states
      |v::q -> aux (for_all states q) L.null v
    in
    try 
      let _ = for_all states_init (vertices g) in false 
    with Cycle -> true

(* Pas encore teste: *)
  exception Exists
  let exist_path (u: L.t) (v: L.t) (g: t): bool = (* test l'existence d'un chemin de u vers v *)
    let states_init = List.fold_left (fun m v -> Lmap.add v NonVisited m) Lmap.empty (vertices g) in
    let rec aux states parent v' =
      if v'=v then (raise Exists) else
      let rec parcours_n: L.t list -> state Lmap.t = function
        |[] -> states
        |w::q -> 
          if parent=w then parcours_n q else
          match Lmap.find w states with
          |Visited |InProgress -> parcours_n q
          |NonVisited -> 
            Lmap.add v' Visited (aux (Lmap.add v' InProgress (parcours_n q)) v' w)
      in
      parcours_n (out_vertices v' g)
      
    in
    try
      let  _ = aux states_init L.null u in false
    with Exists -> true

  type int_barre = Inf | Nb of int (* N ou +oo *)
  let min_barre x y = match x,y with
    |Inf,a | a, Inf -> a
    |Nb a, Nb b -> Nb (min a b)

  let digirth (g: t): int = 
    let states = Hashtbl.create 10 in
    let li_v = vertices g in
    let file = Queue.create () in
    let smallest_cycle (v: L.t): int_barre = (* plus petit cycle passant par v *)
      Queue.clear file;
      Hashtbl.clear states;
      List.iter (fun x -> Hashtbl.add states x NonVisited) li_v;
      Hashtbl.add states v Visited;
      Queue.push (v, 0) file; (* sommet, distance *)
      let rec aux (): int_barre =
        if Queue.is_empty file then Inf else
        let w, d = Queue.pop file in
        if w=v && d > 0 then 
          Nb d
        else
        let f w' = 
          match Hashtbl.find states w' with
           |NonVisited -> 
            Hashtbl.add states w' Visited;
            Queue.push (w', d+1) file 
           |_ -> if w'=v then Queue.push (v,d+1) file else ()
        in
        List.fold_left (fun m w'-> f w') () (out_vertices w g);
        aux ()
      in
      aux ()
    in
    let r = List.fold_left (fun m v -> min_barre m (smallest_cycle v)) Inf li_v in
    match r with
      |Inf -> -1 
      |Nb x -> x

  let read_one ch = (* format digraph6, on suppose que |V(g)| <= 62 *)
    assert ((input_byte ch) = int_of_char '&'); (* on mange & *)
    let nb_vertex = (input_byte ch) - 63 in
    (*Format.printf "nb_vertex = %d@." nb_vertex;*)
    let mat = input_line ch in (* matrice d'adjacence *)
    let result = ref empty in
    for j=0 to nb_vertex -1 do
      result := add_node (L.of_int j) (!result)
    done;
    for j=0 to nb_vertex - 1 do (* lignes *)
      for i=0 to nb_vertex - 1 do (* colonnes *)
        let nb = nb_vertex * j + i in
        let index = nb mod 6 in
        (*Format.printf "coor = %d %d@." (nb / 6) (index);*)
        let character = int_of_char (String.get mat (nb / 6)) - 63 in
        (*Format.printf "char=%d@." (character+63);*)
        let matij = (character  lsr (5-index)) land 1 in
        (*Format.printf "mat(%d,%d) = %d@." i j matij;*)
        if matij = 1 
          then result := add_edge (L.of_int i, L.of_int j) (!result)
          else ()
      done;
    done;
    !result

  let write_one (ch: out_channel) (g: t): unit = (* format digraph6 *)
    let nb_vertex = (List.length (vertices g)) in
    let taille = ceiling (nb_vertex*nb_vertex) 6 in (* nb de char necessaires *)
    let res = Array.make taille 0 in
    for i=0 to nb_vertex -1 do
      for j=0 to nb_vertex - 1 do
        let v =
           if is_adj (L.of_int i) (L.of_int j) g then 1 else 0
        in
        let nb = i*nb_vertex + j in
        let index_char = nb/6 in
        let index = nb mod 6 in
        res.(index_char) <- res.(index_char) lor (v lsl (5-index));
      done;
    done;
    output_char ch '&';
    output_byte ch (63 + nb_vertex);
    Array.iter (fun c -> output_byte ch (c+63)) res;
    output_string ch "\n";
    flush ch

  let rec is_kcol = function (* test de la k-colorabilite *)
    |0 -> fun g -> g=empty
    |1 -> fun g -> not (has_cycle g)
    |n -> fun g ->
      let rec parcours_sub (f: L.t -> bool) l =
        (* f: pour separer les ensembles; l: liste de sommets *)
        let h1 = filter f g in 
        (* si on a deja un cycle, on peut s'arreter tout de suite *)
        if has_cycle h1 then false else
        (match l with
         |[] -> (* on verifie si c'est une (k-1)-col correcte *) 
           let h2 = filter (fun x -> not (f x)) g in
           is_kcol (n-1) h2
         |v::q -> 
           (parcours_sub (fun x -> (x=v) || (f x)) q)
           ||  (parcours_sub f q)
        )
      in
      match vertices g with
        |[] -> true
        |v0::li_v ->
          parcours_sub (fun v -> v=v0) li_v
  (*
  let rec is_kcol = function
    |0 -> fun g -> g=empty
    |1 -> fun g -> not (has_cycle g)
    |k -> fun g ->
      (* on ne teste qu'avec les acycliques maximaux *)
      let rec acyclic_max li (f: L.t -> bool) = function
        |[] -> f::li
        |v::q ->
          let f_addv = fun x -> (x=v) || (f x) in
          let h1 = filter f_addv g in
          if not (has_cycle h1) then
            acyclic_max (acyclic_max li f q) f_addv q
          else
            acyclic_max li f q
       in
       let rec parcours = function
         |[] -> false
         |acyclic::q ->
           let h2 = filter (fun x -> not (acyclic x)) g in
           (is_kcol (k-1) h2) || (parcours q)
       in
       parcours (acyclic_max [] (fun _ -> false) (vertices g))
    *)    

(* (* version avec des Set *)
    let rec parcours_sub s l =
      let h1 = filter (fun x -> Lset.mem x s) g in
      if has_cycle h1 then false else
      (match l with
       |[] -> (* on verifie si c'est un 2col valable *) 
         let h2 = filter (fun x -> not (Lset.mem x s)) g in
         not (has_cycle h2)
       |v::q -> 
         (parcours_sub (Lset.add v s) q)
         ||  (parcours_sub s q)
      )
    in
    parcours_sub (Lset.empty) (vertices g)
*)

  let to_latex (fmt: Format.formatter) (g: t): unit =
     let rec add_vert (fmt: Format.formatter): L.t list -> unit = function
       |[] -> ()
       |[v] -> Format.fprintf fmt "%a" L.print v
       |v::q -> Format.fprintf fmt "%a, %a" L.print v add_vert q
     in
     let rec add_edges fmt = function
       |[] -> ()
       |(v,w)::q -> Format.fprintf fmt "  \\Edges (%a,%a)@.%a" L.print v L.print w add_edges q
     in
     Format.fprintf fmt
"\\begin{tikzpicture}[scale=3]
  \\GraphInit[vstyle=Hasse]
  \\SetVertexLabel
  \\SetUpEdge[style={->}]
  \\SetGraphUnit{0.75}
  \\Vertices{circle}{%a}
  %a
\\end{tikzpicture}"
        add_vert (vertices g)
        add_edges (edges g)

  let aux_di_to_nono (f: G.t -> G.t list): t -> t list = fun g ->
    let nono =
      List.fold_left
       (fun t (x,y) -> G.add_edge (x,y) t)
       (List.fold_left (fun t x -> G.add_node x t) G.empty (vertices g))
       (edges g)
    in
    let li_bcc = f nono in
    let li_dibcc = (* on repasse en oriente *)
      List.map 
       (fun bcc ->  
         let rec aux = function
          |[] -> empty
          |(x,y)::q ->  
            let t = add_node x (add_node y (aux q)) in
            if is_adj x y g then add_edge (x,y) t
            else
            if is_adj y x g then add_edge (y,x) t
            else t
         in
         aux (G.edges bcc))
       li_bcc
     in
     li_dibcc 

  let get_2cc (g: t): t list = aux_di_to_nono G.get_2cc g
  let get_cc  (g: t): t list = aux_di_to_nono G.get_cc g

  let is_connected (g: t): bool =
    let nono =
      List.fold_left
       (fun t (x,y) -> G.add_edge (x,y) t)
       (List.fold_left (fun t x -> G.add_node x t) G.empty (vertices g))
       (edges g)
    in
    G.is_connected nono
    

  let is_cycle g =
    List.for_all (fun x -> (in_degree x g = 1) && (out_degree x g = 1)) (vertices g) &&
    is_connected g

  let is_cactus (g: t): bool =
    List.for_all 
      (fun bcc -> List.for_all 
                    (fun x -> ((in_degree x bcc)=1) && ((out_degree x bcc)=1)) 
                    (vertices bcc)
      )
      (get_2cc g)

  let is_kcritical (k: int) (g: t): bool =
    (not (is_kcol (k-1) g)) &&
    List.for_all (fun e -> is_kcol (k-1) (remove_edge e g)) (edges g)

end


(*
module D(L: LABEL): DIGRAPH_BASIC = struct
  include Graph_Digraph(L)

  let add_edge (v1,v2) g =
    let nv2 = Lset.add v1 (Lmap.find v2 g) in
    Lmap.add v2 nv2 g

  let neighboorhood v g = (* dans le sens successeurs *)
    let n = Lmap.find v g in
    Lset.fold (fun w l -> w::l) n []      


end
*)
module D(L: LABEL): DIGRAPH_BASIC with module L=L = struct
  (* avec deux listes d'adjacences *)
  module L = L
  module Lmap = Map.Make(L)
  module Lset = Set.Make(L)
  type vertex = {in_vertices: Lset.t; out_vertices: Lset.t}
  type t = vertex Lmap.t (* par list d'adjacence *)

  let empty = Lmap.empty

  let isolated_vertex = {in_vertices=Lset.empty; out_vertices=Lset.empty}
  let add_node v g = Lmap.add v isolated_vertex g

  let out_vertices (v: L.t) (g: t): L.t list = 
    Lset.fold (fun w l -> w::l) (Lmap.find v g).out_vertices []

  let in_vertices v g =
    Lset.fold (fun w l -> w::l) (Lmap.find v g).in_vertices []

  let vertices g = Lmap.fold (fun v s l -> v::l) g []

  let filter (cond: L.t -> bool) (g: t): t =
    Lmap.fold 
      (fun v s g' -> 
        (if cond v then
          let new_s = {in_vertices = Lset.filter cond s.in_vertices;
                       out_vertices = Lset.filter cond s.out_vertices}
         in
          Lmap.add v new_s g'
         else g'))
      g empty

  let is_adj v w g = Lset.mem w (Lmap.find v g).out_vertices

  let add_edge ((v: L.t),(w: L.t)) (g: t): t =
    let v_n = Lmap.find v g in
    let w_n = Lmap.find w g in
    Lmap.add v {v_n with out_vertices = Lset.add w v_n.out_vertices}
    (Lmap.add w {w_n with in_vertices  = Lset.add v w_n.in_vertices}
              g)

  let remove_edge ((v: L.t), (w: L.t)) (g: t): t =
    let v_n = Lmap.find v g in
    let w_n = Lmap.find w g in
    Lmap.add v {v_n with out_vertices = Lset.remove w v_n.out_vertices}
    (Lmap.add w {w_n with in_vertices = Lset.remove v w_n.in_vertices}
              g)

end

module Dint: DIGRAPH_BASIC with module L=Intlabel = struct 
  (* avec une liste d'adjacence codee par un int *)
  module L = Intlabel
  module Lmap = Map.Make(L)
  type t = int Lmap.t

  let nb_bits = 15 (* nb maximal de sommet ATTENTION !!!!!!!!!!!!!!!!!!!!!!!!!!!!! *)

  let empty = Lmap.empty

  let add_node v g = Lmap.add v 0 g 

  let add_edge (v1,v2) g =
    let old_v1 = Lmap.find v1 g in
    Lmap.add v1 (old_v1 lor (1 lsl v2)) g

  let remove_edge (v1,v2) g =
    let old_v1 = Lmap.find v1 g in
    Lmap.add v1 (old_v1 land (lnot (1 lsl v2))) g

  let is_adj v1 v2 g =
    let adj_v1 = Lmap.find v1 g in
    0 <> (adj_v1 land (1 lsl v2))

  let vertices g = Lmap.fold (fun v s l -> v::l) g []

  let filter f g =
    let rec make_filter k = 
      if k = nb_bits - 1 then 0 else
        if f k
        then (1 lsl k) lor (make_filter (k + 1))
        else make_filter (k + 1)
    in
    let filt = make_filter 0 in
    Lmap.fold 
      (fun v s g' -> 
        (if f v then
          Lmap.add v (s land filt) g'
         else g'))
      g empty

  let out_vertices v g =
    let rec aux k =
      if k = nb_bits - 1 then [] else
        if is_adj v k g then k::(aux (k+1)) else aux (k+1)
    in aux 0

  let in_vertices v g =
    let rec aux k =
      if k = nb_bits - 1 then [] else
        if is_adj k v g then k::(aux (k+1)) else aux (k+1)
    in aux 0

end

(* This part is not finished *)

type contrainte =
  {d_in_min: int option;  
   d_out_min: int option}

type 'a continuation = 'a list

module OrientationsGenerator(G: GRAPH)(D: DIGRAPH with module L = G.L): sig
    val generate: out_channel -> contrainte -> G.t -> unit
  end = struct

  module L = G.L
  module LMap = Map.Make(L)

  let (<==) a = function
    |None -> true
    |Some b -> a<=b
  let (>==) a = function
    |None -> true
    |Some b -> a>=b

  let generate (oc: out_channel) (cont: contrainte) (g: G.t): unit =
    let d_map = 
      List.fold_left
       (fun m x -> LMap.add x (G.degree x g) m)
       LMap.empty
       (G.vertices g)
    in 
    let rec aux (do_map: (int*int) LMap.t) (d: D.t): (G.L.t * G.L.t) list -> unit = function
      |[] -> D.write_one oc d; flush oc
      |(x,y)::q ->
        let dx = LMap.find x d_map in
        let dy = LMap.find y d_map in
        let ix, ox = LMap.find x do_map in
        let iy, oy = LMap.find y do_map in
        let restx = dx - ox - ix in
        let resty = dy - oy - iy in
       (if (restx + ix - 1  >== cont.d_in_min) && (resty + oy - 1  >== cont.d_out_min)
        then
          let d1 = D.add_edge (y,x) d in
          let do1 = LMap.add x (ix+1,ox) do_map in
          let do2 = LMap.add y (iy,oy+1) do1 in
          aux do2 d1 q
        else
          ()
        );
       (if (resty + iy - 1  >== cont.d_in_min) && (restx + ox - 1  >== cont.d_out_min)
        then
          let d1 = D.add_edge (x,y) d in
          let do1 = LMap.add y (iy+1,oy) do_map in
          let do2 = LMap.add x (ix,ox+1) do1 in
          aux do2 d1 q
        else
          ()
        );
    in
    let ver = G.vertices g in
    aux 
      (List.fold_left
        (fun m x -> LMap.add x (0,0) m)
        LMap.empty ver)
      (List.fold_left
        (fun d x -> D.add_node x d) 
        D.empty ver)
      (G.edges g);


end


