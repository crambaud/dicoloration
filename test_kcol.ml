open Graph


module L = Intlabel
module G = MakeDigraph(D(L))


let () =
  let i = ref 0 in
  let k = int_of_string (Sys.argv.(1)) in
  try
    while true do
      incr i; 
      (if !i mod 100000 = 0 then Format.eprintf "%d@." (!i));
      let g = G.read_one stdin in
      if not (G.is_kcol k g) then
       (Format.eprintf "Found! @.";
        G.write_one stdout g)
    done;
  with End_of_file -> Format.eprintf "Done.@.";


