all: *.ml *.mli
	ocamlbuild -tag debug test_kcol.native
	ocamlbuild -tag debug filtre_arboricite.native
	ocamlbuild -tag debug filtre_cactus.native
	ocamlbuild -tag debug digraph_to_latex.native
	ocamlbuild -tag debug is_kcritical.native

clean:
	rm -r _build *.native


