open Graph

(* ce programme verifie si les graphe induit par les sommets de degre donne est 
  un graphe dont les bloc sont des aretes ou des cycles *)


module L = Intlabel
module G = MakeGraph(G(L))

let is_cactus (g:G.t): bool =  G.is_cactus g
(*
    let e = List.length (G.edges g) in
    let v = List.length (G.vertices g) in
    v = 0 ||
    (e <= 3 *( v-1))
(* ATTENTION , temporaire !!!!!!!!!!! *)
*)

let () =
  let i = ref 0 in
  let dmin = int_of_string Sys.argv.(1) in (* degre minimal *)
  try
    while true do
      incr i; 
      (if !i mod 100 = 0 then Format.eprintf "%d@." (!i));
      let g = G.read_one stdin in
      let t = G.filter (fun x -> (G.degree x g)<=dmin) g in
      if is_cactus t then
       (G.write_one stdout g);
    done;
  with End_of_file -> Format.eprintf "Done.@.";


