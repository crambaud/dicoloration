(** A generic graphs implementation *)
(** This library gives generic implementation of non oriented graphs and digraphs *)

(** Module type [LABEL] gives a general interface to manipulate node's labels *)
module type LABEL =
  sig
    (** Represent any node's label *)
    type t

    (** Used to check if two labels are equal *)
    val compare : t -> t -> int

    (** Used to construct graphs *)
    val of_int : int -> t

    (** function used by the pretty printer *)
    val print : Format.formatter -> t -> unit

    (** a sepcial value used to represent an inexistant node *)
    val null : t
  end

(** This module type gives the elementary function to manipulate non oriented graphs *)
module type GRAPH_BASIC =
  sig
    (** the label module used to label the nodes *)
    module L : LABEL

    (** the type of a graph *)
    type t

    (** the empty graph *)
    val empty : t

    (** adds any node to any graph *)
    val add_node : L.t -> t -> t

    (** adds any edge  between two existant nodes *)
    val add_edge : L.t * L.t -> t -> t

    val remove_edge : L.t * L.t -> t -> t

    (** tests if any edge is in the graph *)
    val is_adj : L.t -> L.t -> t -> bool

    (** returns the list of the vertices of the graph *)
    val vertices : t -> L.t list

    (** returns the sub graph induced by the node for which the test returns true *)
    val filter : (L.t -> bool) -> t -> t

    (** returns the list of the neighboors of any node *)
    val neighboorhood : L.t -> t -> L.t list
  end

(** This is a more general module, with usefull functions *)
module type GRAPH =
  sig
    include GRAPH_BASIC

    (** returns the degree of any vertex *)
    val degree : L.t -> t -> int

    (** read in the in_channel any graph in graph6 format *)
    val read_one : in_channel -> t

    (** write in the out_channel any graph in graph6 format *)
    val write_one : out_channel -> t -> unit

    (** pretty printer *)
    val print : Format.formatter -> t -> unit

    (** check if any graph is stable *)
    val is_stable : t -> bool

    (** check if any graph is complete *)
    val is_clique : t -> bool

    (** tests if any graph has any cycle *)
    val has_cycle : t -> bool

    (** returns the list of edges. Warning: each edge appears two times ((x,y) and (y,x)) *)
    val edges : t -> (L.t * L.t) list

    (** compute the arboricity of any graph *)
    val arboricity : t -> int

    val get_biforest : t -> L.t list

    (** test if any graph is a cycle *)
    val is_cycle: t -> bool

    (** return the connected component of any vertex *)
    val get_one_cc: t -> L.t -> L.t list

    (** test if any graph is connected *)
    val is_connected: t -> bool

    (** returns the list of the cut nodes *)
    val get_cut_nodes: t -> L.t list

    (** returns the list of vertices in each biconnected components of any graph *)
    val get_2cc: t -> t list 

    (** return the connected components of any graph *)
    val get_cc: t -> t list

    (** check uf any graph is a cactus *)
    val is_cactus: t -> bool

    val to_latex: Format.formatter -> t -> unit

  end

module type DIGRAPH_BASIC =
  sig
    module L : LABEL
    type t
    val empty : t
    val add_node : L.t -> t -> t
    val add_edge : L.t * L.t -> t -> t
    val remove_edge : L.t * L.t -> t -> t
    val is_adj : L.t -> L.t -> t -> bool
    val vertices : t -> L.t list
    val filter : (L.t -> bool) -> t -> t
    val out_vertices : L.t -> t -> L.t list
    val in_vertices : L.t -> t -> L.t list
  end

module type DIGRAPH =
  sig
    include DIGRAPH_BASIC
    val print : Format.formatter -> t -> unit
    val out_degree : L.t -> t -> int
    val in_degree : L.t -> t -> int
    val degree : L.t -> t -> int
    val min_out_degree : t -> int
    val min_in_degree : t -> int
    val edges: t -> (L.t * L.t) list
    val read_one : in_channel -> t
    val write_one : out_channel -> t -> unit
    val has_cycle : t -> bool
    val is_kcol : int -> t -> bool
    val reverse : t -> t
    val digirth : t -> int
    val to_latex: Format.formatter -> t -> unit
    val get_cc: t -> t list
    val get_2cc: t -> t list
    val is_connected: t -> bool
    val is_cycle: t -> bool
    val is_cactus: t -> bool
    val is_kcritical: int -> t -> bool
  end

(* val ceiling : int -> int -> int *)

module Intlabel : LABEL with type t=int

module MakeGraph :
  functor (G : GRAPH_BASIC) ->
    GRAPH with module L=G.L

module MakeDigraph :
  functor (G : DIGRAPH_BASIC) ->
    DIGRAPH with module L=G.L

module G :
  functor (L : LABEL) ->
    GRAPH_BASIC with module L=L

module D :
  functor (L : LABEL) ->
    DIGRAPH_BASIC with module L=L

module Dint :
    DIGRAPH_BASIC with module L=Intlabel

module Gint :
    GRAPH_BASIC with module L=Intlabel

type contrainte = { d_in_min : int option; d_out_min : int option; }

type 'a continuation = 'a list

module OrientationsGenerator :
  functor
    (G : GRAPH) (D : DIGRAPH with module L=G.L) ->
    sig val generate : out_channel -> contrainte -> G.t -> unit end

