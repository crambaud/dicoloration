open Graph


module L = Intlabel
module G = MakeGraph(G(L))


let () =
  let i = ref 0 in
  let amin = int_of_string Sys.argv.(1) in (* arboricite minimale *)
  try
    while true do
      incr i; 
      (if !i mod 100 = 0 then Format.eprintf "%d@." (!i));
      let g = G.read_one stdin in
      let arb = G.arboricity g in
      (* Format.eprintf "Arboricity = %d@." arb; *)
      if arb>amin then
       (G.write_one stdout g);
    done;
  with End_of_file -> Format.eprintf "Done.@.";


