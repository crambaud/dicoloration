#!/bin/bash

pick="pickg -d2: -u2: -C"
main="./is_kcritical.native 3"

geng -C -d4 $@ |./filtre_cactus.native 4 | ./filtre_arboricite.native 2 | \
  directg -o | \
  tee  >(awk 'NR % 4 == 0' | $pick | $main) \
       >(awk 'NR % 4 == 1' | $pick | $main) \
       >(awk 'NR % 4 == 2' | $pick | $main) \
       >(awk 'NR % 4 == 3' | $pick | $main) \
       > /dev/null
