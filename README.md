Petit programme permettant de tester la 2-dicolorabilité de graphes orientés.

## Dépendances ##
- Ocaml: j'utilise la version 4.07, mais une plus ancienne ne devrait pas poser de problème.
- plantri: permet de générer des triangulations du plan 
           ([lien](https://users.cecs.anu.edu.au/~bdm/plantri/))
- surftri: permet de générer des triangulations d'une surface presque quelconque
           ([lien](http://pages.iu.edu/~tsulanke/graphs/surftri/))
- nauty: divers outils sur les graphes orientés ou non
           ([lien](http://users.cecs.anu.edu.au/~bdm/nauty/)).

## Compilation ##
Taper `make`.

## Utilisation ##

Pour tester la 2-dicolorabilité des triangulations du plan projectif de 
degré minimum 5 et à 8 sommets:
```
surftri -g -n -m5 8 1 | nauty-directg -o | ./test_kcol.native 2
```
Explications: surftri génère les triangulations à 8 sommets
du plan projectif (`-n` pour non orienté, et `1` car de genre 1)
de degré min 5 (`-m5`), puis `nauty-directg` calcule toutes les
orientations de ces dernières. Enfin `./test_kcol.native` teste la 2-dicolorabilité.


Pour tester la 2-dicolorabilité des triangulations du tore de 
degré minimum 5 et à 10 sommets:
```
surftri -g -m5 10 1  | nauty-directg -o | ./test_kcol.native 2
```

### Nombre dichromatique du triple plan projectif:
Le script `third_projective_plane.sh` permet de montrer que tout
graphe orienté plongé dans $\mathbb{N}_3$ est $3$-dicolorable.
Un éventuel contre exemple serai affiché sur la sortie standard.

## Formats ##
Tous ces programme communiquent en utilisant le format graph6 et digraph6 
([lien](https://users.cecs.anu.edu.au/~bdm/data/formats.txt)).
On peut les lire avec `nauty-showg`.

## Code Ocaml ##

Le fichier `graph.ml` contient plusieurs modules et foncteurs permettant une représentation
plutot générique d'un graphe.

### Algorithme pour le test de la dicolorabilité ###
On fait un test récursif, en testant (presque) toutes les partitions possibles.

### Complexité ###
Il y a $\mathcal{O}(2^{3v})$ orientations d'une triangulation à $v$ sommets.

La $k$-colorabilité d'un grapheo orienté est testée de façon  naïve en $O(k^v)$.

Cette implémentation est un peu naïve, on pourrait peut-être imaginer
un algorithme qui ne teste pas les orientations indépendament.



