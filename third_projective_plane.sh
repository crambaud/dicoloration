# teste la 3-colorabilite des graphe d'ordre entre 12 et 13 dans N_3

# si le sous graphe des sommets de petit degre est une cactus,
# puis on teste l'arboricite, puis on oriente, puis on teste degre entrant et sortant,
# puis on teste la 3-colorabilite 
echo "test des 2037 triangulations de taille 12:"
time surftri -n -m6 12 3 -g | nauty-pickg -e39:39 | ./filtre_cactus.native 6 | ./filtre_arboricite.native 3 \
   | nauty-directg -o | nauty-pickg -C -d3 -u3 | ./test_kcol.native 3


echo "test des 4574 triangulations de taille 13:"
surftri -n -m6 13 3 -g | nauty-pickg -e42:42 | ./filtre_cactus.native 6 | ./filtre_arboricite.native 3 \
   | nauty-directg -o | nauty-pickg -C -d3 -u3 | ./test_kcol.native 3

