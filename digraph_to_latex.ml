open Graph


module L = Intlabel
module G = MakeDigraph(D(L))


let () =
  try
    while true do
      let g = G.read_one stdin in
      Format.printf "%a @.%%--------------------------@.@."
        G.to_latex g
    done;
  with End_of_file -> (); 


